<style type="text/css">
#djresidents{
    float: left;
    margin-left: 190px;
    padding: 10px;
    margin-right: 58px;
}
#djresidents-heading, #guests-heading{
font-size: 23px;
}
#djguests{
padding: 10px;
}
#suggestlist_rg{
    margin-left: 159px;
}
.crossImage{
    float:right;
    height: 15px; 
    cursor:pointer;
}
#djlink{
    float:left; 
    cursor:pointer;
}
#djguests{
float:left;
}
#residents_dj, #guests_dj{
    border: solid 1px yellow;
    margin-top: 3px;
}
</style>
<h2>Mushroom Club Resident DJs</h2>

<div id="djresidents">
    <div id="djresidents-heading">Residents</div>
    <ul id="residents_list">
        <?php if(count($this->residentdjs)): ?>
            <?php foreach($this->residentdjs as $dj): ?>
            <li id='djresident_<?php echo $dj->dj_id; ?>'><span><a href='/page/<?php echo $dj->url; ?>' target='_blank' id="djlink"><?php echo $dj->name; ?></a></span><img src="public/admin/cross.png" class="crossImage" id='djresident_cross_<?php echo $dj->dj_id; ?>' onclick="rdeletedj(this)" rev='<?php echo $dj->dj_id; ?>' del='<?php echo $dj->residentsguest_id; ?>' typerg="1"><div class="clear"></div></li> 
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
    <ul>
        <li><input type="text" name="residents_dj" id="residents_dj" onkeyup="djrsuggestions(this);"></li>  
        <li><div id="suggestlist_rg" class="suggestion_list"></div></li>  
    </ul>
</div>

<div id="djguests">
     <div id="guests-heading">Guests</div>
     <ul id="guests_list">
         <?php if(count($this->guestdjs)): ?>
            <?php foreach($this->guestdjs as $dj): ?>
            <li id='djresidentg_<?php echo $dj->dj_id; ?>'><span><a href='/page/<?php echo $dj->url; ?>' target='_blank' id="djlink"><?php echo $dj->name; ?></a></span><img src="public/admin/cross.png" class="crossImage" id='djresident_cross_<?php echo $dj->dj_id; ?>' onclick="rdeletedj(this);" rev='<?php echo $dj->dj_id; ?>' del='<?php echo $dj->residentsguest_id; ?>' typerg="2"><div class="clear"></div></li> 
            <?php endforeach; ?>
        <?php endif; ?>
     </ul>
    <ul>
        <li><input type="text" name="guests_dj" id="guests_dj" onkeyup="djgsuggestions(this);"></li>  
        <li><div id="suggestlist_g" class="suggestion_list"></div></li>  
    </ul> 
     
</div>

<script type="text/javascript">

function djrsuggestions(field){
            console.log('djsuggestions()');
            var val = field.value;
            var url = en4.core.baseUrl + 'heevent/index/dj-suggestion';
            var fieldId = field.id;
            console.log('Field Id: '+fieldId);
            var boxid = 'suggestlist_rg';
            console.log('input boxid: '+boxid);
            var boxElement = $(boxid);
                    var req = new Request.HTML({
                        url : url,
                        data : {
                          format :'html',
                          keyword: val
                        },
                        onComplete : function(responseTree, responseElements, responseHTML, responseJavaScript) {  
                          if(responseHTML){
                                boxElement.set('html',"");
                                boxElement.set('html',responseHTML);
                                boxElement.style.display="block";
                                boxElement.getElements('a').each(function(el) {
                                             el.set('onclick','adddjrg(this,1)');
                                             el.set('id',fieldId);
                                });
                            }else{
                                if(field.value.length>0){
                                    
                                    boxElement.set('html','<button style="width: inherit;" id="'+fieldId+'" onclick="addNewDjPager(event,this,1);">Add New Dj.</button>');
                                    boxElement.style.display="block";    
                                    }else{
                                        boxElement.set('html','<p onclick="hidePopup(this);">No result found.</p>');
                                        boxElement.style.display="block";    
                                         }
                                
                            }

                          },onSuccess : function(data) {

                          }
                    });
                req.send();    
        }
function adddjrg(el,val){
            console.log('element: '+el);
            console.log('val: '+val);
            var djname = el.get('rel');
            var djid = el.get('djid');
            var djpageurl = el.get('pageurl');
            console.log('djid: '+djid);
                if(djname.length>20){
                djname = djname.substring(0, 18);
                djname = djname+'..';
                }
            var djresident_dynamic = "djresident_"+djid;
            var djresident_cross_dynamic = "djresident_cross_"+djid;
            
            var page_id = jqcc('#page_id').val();
            var type = 1;
            //saveAddDjsResident(djid,page_id,type);
            
            //Save Dj
             console.log('saveAddDjsResident: '+djid+' '+'page_id: '+page_id+' type: '+type);
          
            var url = '<?php echo $this->url(array('module' => 'user', 'controller' => 'radio-formating', 'action' => 'save-residentdj'), 'default', true) ?>';
            var boxid = 'suggestlist_rg';
            console.log('input boxid: '+boxid);
            var boxElement = $(boxid);
            boxElement.set('html',"");
            boxElement.style.display="none";
            var req = new Request.JSON({
                url : url,
                data : {
                  format : 'json',
                  dj_id : djid,
                  page_id : page_id,
                  type : type,
                },
                onComplete : function(data) {  
                  },onSuccess : function(data) {
                    var success = data[0].success;
                    if(success===true){
                    console.log('success');
                    var residentsguest_id = data[0].info_id;
                    var djnamelink = '<li id="'+djresident_dynamic+'"><span><a href="'+djpageurl+'" target="_blank" id="djlink">'+djname+'</a></span><img src="public/admin/cross.png" class="crossImage" id="'+djresident_cross_dynamic+'" rev="'+djid+'" onclick="rdeletedj(this)" typerg="1" del="'+residentsguest_id+'"><div class="clear"></div></li>'; 
                    console.log('djnamelink: '+djnamelink);
                    jqcc("#residents_list").append(djnamelink);
                    document.getElementById('residents_dj').value="";
                    }else{
                        console.log('problem in saving dj');
                        alert('Dj already Exists');
                    }
                  }
            });
            req.send();    
            
        }
function rdeletedj(crosselem){
        console.log('rdeleetedj');
        console.log(crosselem);
        var el = '#'+crosselem.id;
        var type = crosselem.get('typerg');
        console.log(type);
        if(type == 1){
             var parent_id = 'djresident_'+crosselem.get('rev');
        } else {
             var parent_id = 'djresidentg_'+crosselem.get('rev');
        }
       
       
        console.log('Parent_id: '+parent_id);
        document.getElementById(parent_id).style.display="none";  
        var residentsguest_id = crosselem.get('del');
        
        console.log("residentsguest_id delete: "+residentsguest_id);
        
        
        //Delete Resident DJ
        var url = '<?php echo $this->url(array('module' => 'user', 'controller' => 'radio-formating', 'action' => 'del-residentdj'), 'default', true) ?>';
            var req = new Request.JSON({
                url: url,
                data : {
                format:'json',
                residentsguest_id:residentsguest_id
                },
                onComplete : function(data) {  
                },onSuccess : function(data) {
                console.log(data.success);
                }
            });
            req.send();
    }    
function addNewDjPager(ev,elm,type){
            ev.preventDefault();
            var fieldId = elm.id;
            var djname = document.getElementById(elm.id).value;
            var page_id = jqcc('#page_id').val();
            console.log('djname: '+djname);
            console.log(elm);
            if(type==1){
                document.getElementById('suggestlist_rg').style.display="none";
            }else{
                document.getElementById('suggestlist_g').style.display="none";
            }
            var url = '<?php echo $this->url(array('module' => 'user', 'controller' => 'radio-formating', 'action' => 'newresidentdj-page'), 'default', true) ?>';
            var req = new Request.HTML({
                url : url,
                data : {
                      format : 'html',
                      djname : djname,
                      page_id : page_id,
                      type : type
                },onComplete :  function(responseTree, responseElements, responseHTML, responseJavaScript)  {  
                    console.log('djnamelink: '+responseHTML);
                    
                        if(type == 1){
                            jqcc("#residents_list").append(responseHTML);
                            document.getElementById('residents_dj').value="";
                        }else{
                            jqcc("#guests_list").append(responseHTML);
                            document.getElementById('guests_dj').value="";
                        }
                    },onSuccess : function(data) {

                    }
                });
                req.send();
}    
    
    
function djgsuggestions(field){
            console.log('djgsuggestions()');
            var val = field.value;
            var url = en4.core.baseUrl + 'heevent/index/dj-suggestion';
            var fieldId = field.id;
            console.log('Field Id: '+fieldId);
            var boxid = 'suggestlist_g';
            console.log('input boxid: '+boxid);
            var boxElement = $(boxid);
                    var req = new Request.HTML({
                        url : url,
                        data : {
                          format :'html',
                          keyword: val
                        },
                        onComplete : function(responseTree, responseElements, responseHTML, responseJavaScript) {  
                          if(responseHTML){
                                boxElement.set('html',"");
                                boxElement.set('html',responseHTML);
                                boxElement.style.display="block";
                                boxElement.getElements('a').each(function(el) {
                                             el.set('onclick','adddjg(this,2)');
                                             el.set('id',fieldId);
                                });
                            }else{
                                if(field.value.length>0){
                                    
                                    boxElement.set('html','<button style="width: inherit;" id="'+fieldId+'" onclick="addNewDjPager(event,this,2);">Add New Dj.</button>');
                                    boxElement.style.display="block";    
                                    }else{
                                        boxElement.set('html','<p onclick="hidePopup(this);">No result found.</p>');
                                        boxElement.style.display="block";    
                                         }
                            }

                          },onSuccess : function(data) {

                          }
                    });
                req.send();    
        }
function adddjg(el,val){
            console.log('element: '+el);
            console.log('val: '+val);
            var djname = el.get('rel');
            var djid = el.get('djid');
            var djpageurl = el.get('pageurl');
            console.log('djid: '+djid);
            if(djname.length>20){
            djname = djname.substring(0, 18);
            djname = djname+'..';
            }
            var djresident_dynamic = "djresidentg_"+djid;
            var djresident_cross_dynamic = "djresident_cross_"+djid;
            
            var page_id = jqcc('#page_id').val();
            var type = 2;
            //saveAddDjsResident(djid,page_id,type);
            
            //Save Dj
             console.log('saveAddDjsResident: '+djid+' '+'page_id: '+page_id+' type: '+type);
          
            var url = '<?php echo $this->url(array('module' => 'user', 'controller' => 'radio-formating', 'action' => 'save-residentdj'), 'default', true) ?>';
            var boxid = 'suggestlist_g';
            console.log('input boxid: '+boxid);
            var boxElement = $(boxid);
            boxElement.set('html',"");
            boxElement.style.display="none";
            var req = new Request.JSON({
                url : url,
                data : {
                  format : 'json',
                  dj_id : djid,
                  page_id : page_id,
                  type : type,
                },
                onComplete : function(data) {  
                  },onSuccess : function(data) {
                    var success = data[0].success;
                    if(success===true){
                    console.log('success');
                    var residentsguest_id = data[0].info_id;
                    var djnamelink = '<li id="'+djresident_dynamic+'"><span><a href="'+djpageurl+'" target="_blank" id="djlink">'+djname+'</a></span><img src="public/admin/cross.png" class="crossImage" id="'+djresident_cross_dynamic+'" rev="'+djid+'" onclick="rdeletedj(this)" del="'+residentsguest_id+'" typerg="2"><div class="clear"></div></li>'; 
                    console.log('djnamelink: '+djnamelink);
                    jqcc("#guests_list").append(djnamelink);
                    document.getElementById('guests_dj').value="";
                    }else{
                        console.log('problem in saving dj');
                        alert('Dj already Exists');
                    }
                  }
            });
            req.send();    
            
        }
function addNewDjPageg(elm){
alert('working');
}   


</script>    