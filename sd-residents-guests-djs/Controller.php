<?php

class Widget_SdResidentsGuestsDjsController extends Engine_Content_Widget_Abstract
{
  public function indexAction()
  {
        $residentsguests = Engine_Api::_()->getDbtable('residentsguests', 'user');
        $rgName = $residentsguests->Info('name');
        
        $djPage = Engine_Api::_()->getDbtable('pages', 'page');
        $djPageName = $djPage->Info('name');
        $page_id = 0;
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request->getParams();
        $page_id = $params['page_id'];
        
        //View Page
        if($params['action']=='view'){
            $this->view->viewpage = TRUE;
            $page_id = $this->_getParam('page_id');
        }
   
        if(!$page_id) return $this->setNoRender();
        
        
        //fetch residents and guests djs
        $select = $residentsguests->select()
                    ->setIntegrityCheck(false)
                    ->from($rgName, array($rgName . '.*'))
                    ->joinLeft($djPageName, "$djPageName.page_id = $rgName.dj_id", array($djPageName . '.name',$djPageName . '.url'))
                    ->where($rgName.'.page_id = ?', $page_id)
                    ->where($rgName.'.type = ?', 1);
      
        $djs = $residentsguests->fetchAll($select);
             
        $this->view->residentdjs = $djs;
        
        $selectDataGuests = $residentsguests->select()
                    ->setIntegrityCheck(false)
                    ->from($rgName, array($rgName . '.*'))
                    ->joinLeft($djPageName, "$djPageName.page_id = $rgName.dj_id", array($djPageName . '.name',$djPageName . '.url'))
                    ->where($rgName.'.page_id = ?', $page_id)
                    ->where($rgName.'.type = ?', 2);
       
        $djs = $residentsguests->fetchAll($selectDataGuests);
                
        $this->view->guestdjs = $djs;
  }
}